package com.example.klarna.weatherapp.repository

import android.content.Context
import com.example.klarna.weatherapp.repository.remote.RemoteClient

class RepositoryProvider private constructor(context: Context) {

    private lateinit var remoteClient: RemoteClient

    init {
        val baseDir = context.cacheDir
        if(baseDir != null) {
            remoteClient = RemoteClient(baseDir = baseDir)
        }
    }

    private var weatherRepository: WeatherRepository? = null

    fun getWeatherRepository(): WeatherRepository {
        weatherRepository = weatherRepository ?: WeatherRepository(remoteClient)
        return weatherRepository!!
    }

    companion object {

        @Volatile
        private var instance: RepositoryProvider? = null

        fun getInstance(context: Context): RepositoryProvider {
            instance = instance ?: RepositoryProvider(context)
            return instance!!
        }
    }
}