package com.example.klarna.weatherapp.repository.remote

import android.os.Handler
import android.os.Looper
import com.example.klarna.weatherapp.model.ErrorModel
import com.example.klarna.weatherapp.model.ResponseModel
import com.google.gson.GsonBuilder
import com.google.gson.JsonParser
import com.google.gson.JsonSyntaxException
import okhttp3.*
import okhttp3.HttpUrl.Companion.toHttpUrlOrNull
import java.io.File
import java.io.IOException
import java.util.concurrent.TimeUnit


class RemoteClient(
    connectTimeout: Long = CONNECTION_TIMEOUT_DEFAULT,
    readTimeout: Long = READ_TIMEOUT_DEFAULT,
    cacheSize: Long = HTTP_CACHE_SIZE,
    responseCache: String = HTTP_RESPONSE_CACHE,
    baseDir: File
) {

    private var okHttpClient: OkHttpClient
    private val jsonParser = JsonParser()
    private var gson = GsonBuilder().create()

    init {
        val cacheDir = File(baseDir, responseCache)
        val builder = OkHttpClient().newBuilder()
        builder.cache(Cache(cacheDir, cacheSize))
            .readTimeout(readTimeout, TimeUnit.MILLISECONDS)
            .connectTimeout(connectTimeout, TimeUnit.MILLISECONDS)
        okHttpClient = builder.build()
    }

    fun <T> get(url: String, type: Class<T>, callback: RequestCallback<T>) {
        val httpBuilder = url.toHttpUrlOrNull()!!.newBuilder()
        val request = Request.Builder()
            .url(httpBuilder.build())
            .build()
        val call = okHttpClient.newCall(request)
        call.enqueue(requestCallback(callback, type))
    }

    private fun <T> requestCallback(callback: RequestCallback<T>, type: Class<T>): Callback {

        return object : Callback {
            var mainHandler = Handler(Looper.getMainLooper())

            override fun onFailure(call: Call, e: IOException) {
                mainHandler.post {
                    callback.onNetworkError(e)
                }
            }

            override fun onResponse(call: Call, response: Response) {
                val responseCode = response.code
                try {
                    val body = response.body!!.string()
                    response.body!!.close()

                    val responseJson = jsonParser.parse(body)

                    if (response.isSuccessful) {

                        mainHandler.post {
                            callback.onResponse(
                                ResponseModel(
                                    gson.fromJson(responseJson, type),
                                    responseCode
                                )
                            )
                        }
                    } else {
                        //wouldnt normally hardcode this but just to illustrate what could be returned here
                        mainHandler.post { callback.onError(ErrorModel("Something went wrong!", responseCode)) }
                    }

                } catch (e: IOException) {
                    //wouldnt normally hardcode this but just to illustrate what could be returned here
                    mainHandler.post { callback.onError(ErrorModel("Something went wrong!", responseCode)) }
                } catch (e: JsonSyntaxException) {
                    //wouldnt normally hardcode this but just to illustrate what could be returned here
                    mainHandler.post { callback.onError(ErrorModel("Something went wrong!", responseCode)) }
                }

            }
        }
    }

    companion object {

        private const val CONNECTION_TIMEOUT_DEFAULT = 60000L
        private const val READ_TIMEOUT_DEFAULT = 60000L
        private const val HTTP_CACHE_SIZE = (10 * 1024 * 1024).toLong()

        private const val HTTP_RESPONSE_CACHE = "HttpResponseCache"
    }

    interface RequestCallback<T> {
        fun onResponse(responseModel: ResponseModel<T>)

        fun onError(responseModel: ErrorModel)

        fun onNetworkError(exception: IOException)
    }

}