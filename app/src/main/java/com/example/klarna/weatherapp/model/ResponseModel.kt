package com.example.klarna.weatherapp.model

data class ResponseModel<T>(val data: T, val httpStatusCode: Int)
data class ErrorModel(val errorMessage: String, val httpStatusCode: Int)