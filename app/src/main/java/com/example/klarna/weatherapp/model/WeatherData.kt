package com.example.klarna.weatherapp.model

import androidx.databinding.BaseObservable


class WeatherData : BaseObservable() {
    var temperature: String = ""
    var summary: String = ""
    var time: String = ""
    var day: String = ""
    var precipitationType: String = ""
    var humidity: String = ""
    var windSpeed: String = ""
    var days : List<DayWeather>? = ArrayList()
    var icon: Int = 0
}

class DayWeather {
    var day: String = ""
    var summary: String = ""
    var temperatureMax: String = ""
    var temperatureMin: String = ""
    var icon: Int = 0
}