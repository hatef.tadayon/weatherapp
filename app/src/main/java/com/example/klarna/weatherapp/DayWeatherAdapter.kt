package com.example.klarna.weatherapp

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.klarna.weatherapp.databinding.DayWeatherListRowItemBinding
import com.example.klarna.weatherapp.model.DayWeather


class DayWeatherAdapter(private val context: Context) :
    RecyclerView.Adapter<DayWeatherAdapter.DayViewHolder>() {

    var items: List<DayWeather> = ArrayList()

    fun setWeatherItems(items: List<DayWeather>) {
        this.items = items
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: DayViewHolder, position: Int) {
        holder.bindItems(items[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DayViewHolder {
        val v = LayoutInflater.from(context).inflate(
            R.layout.day_weather_list_row_item,
            parent, false
        )
        return DayViewHolder(v)
    }

    class DayViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(dayWeather: DayWeather) {
            val binding = DataBindingUtil.bind<DayWeatherListRowItemBinding>(itemView)
            binding?.weather = dayWeather
        }
    }

    @BindingAdapter("imageResource")
    fun setImageResource(imageView: ImageView, resource: Int) {
        imageView.setImageResource(resource)
    }
}