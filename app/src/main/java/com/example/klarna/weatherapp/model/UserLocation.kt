package com.example.klarna.weatherapp.model

//Setting default values just for simplicity, would otherwise get this from locationservice
class UserLocation(var longitude: Double = 59.337239, var latitude: Double = 18.062381)
