package com.example.klarna.weatherapp.transformer

import com.example.klarna.weatherapp.R
import com.example.klarna.weatherapp.model.*
import java.text.SimpleDateFormat
import java.util.*

class WeatherDataTransformer {

    var datePatter = "yyyy-MM-dd"
    var pattern = "EEE"
    var TEMP_STRING = "%.1f °"
    var dateFormat = SimpleDateFormat(datePatter, Locale.ENGLISH)
    var dayFormat = SimpleDateFormat(pattern, Locale.ENGLISH)


    fun getWeatherData(responseModel: WeatherDataResponseModel): WeatherData {
        val weatherData = WeatherData()

        responseModel.currently?.let { current ->

            current.temperature?.let { temp ->
                weatherData.temperature = getTemperatureInCelsius(temp)
            }
            current.humidity?.let {
                weatherData.humidity = it.toString()
            }
            current.icon?.let {
                weatherData.icon = getWeatherImage(it)
            }
            current.summary?.let {
                    weatherData.summary = it
            }
            current.windSpeed?.let {
                weatherData.windSpeed = it.toString()
            }
            current.precipType?.let {
                weatherData.precipitationType = it
            }
            current.time?.let {
                val date = Date(it * 1000)
                weatherData.time = dateFormat.format(date)
                weatherData.day = dayFormat.format(date)
            }
        }

        responseModel.daily?.let { daily ->
            weatherData.days = daily.data.map { data -> getDailyData(data) }.toList()
        }

        return weatherData
    }

    private fun getDailyData(responseModel: DailyWeatherDataResponseModel): DayWeather {
        val dayWeather = DayWeather()

        responseModel.temperatureHigh?.let { highTemp ->
            dayWeather.temperatureMax = getTemperatureInCelsius(highTemp)
        }
        responseModel.temperatureLow?.let { lowTemp ->
            dayWeather.temperatureMin = getTemperatureInCelsius(lowTemp)
        }
        responseModel.summary?.let {
            dayWeather.summary = it
        }

        responseModel.time?.let { time ->
            val date = Date(time * 1000)
            dayWeather.day = dayFormat.format(date)
        }

        responseModel.icon?.let { time ->
            dayWeather.icon = getWeatherIcon(time)
        }

        return dayWeather
    }

    private fun getTemperatureInCelsius(temp: Float): String {
        val temperature = ((temp - 32) * 5 / 9)
        return TEMP_STRING.format(temperature)
    }

    private fun getWeatherImage(icon: String): Int{
        if(icon.contains(CLEAR)){
         return R.drawable.art_clear
        } else if(icon.contains(CLOUDY)){
            return R.drawable.art_clouds
        } else if(icon == RAIN){
            return R.drawable.art_rain
        } else if(icon == SLEET || icon == SNOW){
            return R.drawable.art_snow
        } else if (icon == WIND){
            return R.drawable.art_storm
        } else if (icon == FOG){
            return R.drawable.art_fog
        } else {
            //assume weather is always great
            return R.drawable.art_clear
        }
    }

    private fun getWeatherIcon(icon: String): Int{
        if(icon.contains(CLEAR)){
            return R.drawable.ic_clear
        } else if(icon.contains(CLOUDY)){
            return R.drawable.ic_cloudy
        } else if(icon == RAIN){
            return R.drawable.ic_rain
        } else if(icon == SLEET || icon == SNOW){
            return R.drawable.ic_snow
        } else if (icon == WIND){
            return R.drawable.ic_storm
        } else if (icon == FOG){
            return R.drawable.ic_fog
        } else {
            //assume weather is always great
            return R.drawable.ic_clear
        }
    }
}
