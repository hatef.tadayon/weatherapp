package com.example.klarna.weatherapp.model

const val CLEAR: String = "clear"
const val CLOUDY: String = "cloudy"
const val RAIN: String = "rain"
const val SLEET: String = "sleet"
const val SNOW: String = "snow"
const val WIND: String = "wind"
const val FOG: String = "fog"
data class WeatherDataResponseModel(
    val latitude: Double?,
    val longitude: Double?,
    val timezone: String?,
    val currently: WeatherDetailDataResponseModel?,
    val hourly: HourlyWeatherDataResponseModel<WeatherDetailDataResponseModel>?,
    val daily: HourlyWeatherDataResponseModel<DailyWeatherDataResponseModel>?
)


data class WeatherDetailDataResponseModel(
    val time: Long?,
    val summary: String?,
    val icon: String?,
    val precipIntensity: Float?,
    val precipProbability: Float?,
    val precipType: String?,
    val temperature: Float?,
    val apparentTemperature: Float?,
    val dewPoint: Float?,
    val humidity: Float?,
    val pressure: Float?,
    val windSpeed: Float?,
    val windGust: Float?,
    val windBearing: Int?,
    val cloudCover: Float?,
    val uvIndex: Int?,
    val visibility: Int?,
    val ozone: Float?
)

data class HourlyWeatherDataResponseModel<T>(
    val summary: String?,
    val icon: String?,
    val data: List<T>
)

data class DailyWeatherDataResponseModel(
    val time: Long?,
    val summary: String?,
    val icon: String?,
    val sunriseTime: Long?,
    val sunsetTime: Long?,
    val moonPhase: Float?,
    val precipIntensity: Float?,
    val precipIntensityMax: Float?,
    val precipIntensityMaxTime: Long?,
    val precipProbability: Float?,
    val precipType: String?,
    val temperatureHigh: Float?,
    val temperatureHighTime: Long?,
    val temperatureLow: Float?,
    val temperatureLowTime: Long?,
    val apparentTemperatureHigh: Float?,
    val apparentTemperatureHighTime: Long?,
    val apparentTemperatureLow: Float?,
    val apparentTemperatureLowTime: Long?,
    val dewPoint: Float?,
    val humidity: Float?,
    val pressure: Float?,
    val windSpeed: Float?,
    val windGust: Float?,
    val windGustTime: Long?,
    val windBearing: Int?,
    val cloudCover: Float?,
    val uvIndex: Int?,
    val uvIndexTime: Long?,
    val visibility: Int?,
    val ozone: Float?,
    val temperatureMin: Float?,
    val temperatureMinTime: Long?,
    val temperatureMax: Float?,
    val temperatureMaxTime: Long?,
    val apparentTemperatureMin: Float?,
    val apparentTemperatureMinTime: Long?,
    val apparentTemperatureMax: Float?,
    val apparentTemperatureMaxTime: Long?
)