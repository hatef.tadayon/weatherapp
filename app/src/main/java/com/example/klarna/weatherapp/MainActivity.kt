package com.example.klarna.weatherapp

import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.klarna.weatherapp.databinding.ActivityMainBinding
import com.example.klarna.weatherapp.model.UserLocation
import com.example.klarna.weatherapp.model.WeatherData
import com.example.klarna.weatherapp.repository.RepositoryProvider
import com.example.klarna.weatherapp.repository.WeatherResponseCallback
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    lateinit var binding: ActivityMainBinding
    lateinit var adapter: DayWeatherAdapter
    lateinit var userLocation: UserLocation
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        recycler_view.layoutManager = LinearLayoutManager(
            this,
            RecyclerView.VERTICAL, false
        )
        adapter = DayWeatherAdapter(this)
        recycler_view.adapter = adapter

        userLocation = UserLocation()
        fetchWeatherData(userLocation)
    }

    private fun fetchWeatherData(location: UserLocation) {
        val weatherRepository = RepositoryProvider.getInstance(this).getWeatherRepository()
        weatherRepository.getWeatherData(
            latitude = location.latitude,
            longitude = location.longitude,
            callback = object : WeatherResponseCallback {
                override fun onResponse(weatherData: WeatherData) {
                    setupView(weatherData)
                }

                override fun onError(error: String) {
                    showError(error)
                }
            })
    }

    private fun showError(error: String) {
        AlertDialog.Builder(this)
            .setTitle(getString(R.string.failed))
            .setMessage(error)
            .setPositiveButton(R.string.try_again) { _, _ ->
                fetchWeatherData(userLocation)
            }
            .setNegativeButton(android.R.string.no, null)
            .setIcon(android.R.drawable.ic_dialog_alert)
            .show()
    }

    private fun setupView(weatherData: WeatherData) {
        binding.weather = weatherData
        weatherData.days?.let { days ->
            adapter.setWeatherItems(days)
        }

    }

}
