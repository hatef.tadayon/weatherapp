package com.example.klarna.weatherapp.repository

import com.example.klarna.weatherapp.model.*
import com.example.klarna.weatherapp.repository.remote.RemoteClient
import com.example.klarna.weatherapp.transformer.WeatherDataTransformer
import java.io.IOException

class WeatherRepository(var remoteClient: RemoteClient) {

    private val WEATHER_URL = "https://api.darksky.net/forecast/2bb07c3bece89caf533ac9a5d23d8417/%s,%s"
    var weatherDataTransformer: WeatherDataTransformer = WeatherDataTransformer()

    fun getWeatherData(longitude: Double, latitude: Double, callback: WeatherResponseCallback) {

        remoteClient.get(WEATHER_URL.format(longitude, latitude),
            WeatherDataResponseModel::class.java,
            object : RemoteClient.RequestCallback<WeatherDataResponseModel> {
                override fun onResponse(responseModel: ResponseModel<WeatherDataResponseModel>) {
                    callback.onResponse(weatherDataTransformer.getWeatherData(responseModel = responseModel.data))
                }

                override fun onError(responseModel: ErrorModel) {
                    callback.onError(responseModel.errorMessage)
                }

                override fun onNetworkError(exception: IOException) {
                    callback.onError("Connection failed, Check your network connection!")
                }
            })
    }

}

//Probably would use rxjava to return an observable instead of this
// in order to not recreate an interface per request
interface WeatherResponseCallback {
    fun onResponse(weatherData: WeatherData)
    fun onError(error: String)
}
